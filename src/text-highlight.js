import getSentences from './ext/sbd'

// used to distinguish between browser and Node.js environments
const isBrowser =
  typeof window !== 'undefined' && typeof window.document !== 'undefined'

// id pattern can be customized
const OPEN_HIGHLIGHT_TAG = (highlightIdPattern, highlightClass) =>
  `<span id="${highlightIdPattern}" class="${highlightClass}">`
const CLOSE_HIGHLIGHT_TAG = '</span>'

class Highlighter {
  constructor(options = {}) {
    // either containerId or content is required
    const containerId = options.containerId
    const content = options.content
    const isHTML = options.isHTML
    const highlightIdPattern = options.highlightIdPattern || 'highlight-'

    if (content) {
      this.originalContent = content
    } else if (isBrowser && containerId) {
      this.originalContent = document.getElementById(containerId).innerHTML
    }
    // isHTML is used to reduce the memory used: stripedHTML is empty if isHTML is false
    this.isHTML = isHTML
    this.highlightIdPattern = highlightIdPattern

    // stripedHTML and tagLocations are needed only when the content is HTML
    this.stripedHTML = ''
    this.tagLocations = []
    // sentences are used in (sentence based) fuzzy search
    this.sentences = []
    // can one highlight have more than one location
    this.highlights = []

    if (isHTML) {
      this.stripAndStoreHTMLTags()
    }
  }

  search(str, options = {}) {
    const prefix = options.prefix || ''
    const postfix = options.postfix || ''
    const directSearchOptions = options.directSearchOptions
    const fuzzySearch = options.fuzzySearch
    const fuzzySearchOptions = options.fuzzySearchOptions
    const eagerSearchOptions = options.eagerSearchOptions

    let highlightIndex = -1

    // direct search will always be performed
    highlightIndex = this.directSearch(
      prefix,
      str,
      postfix,
      directSearchOptions
    )
    if (highlightIndex !== -1) {
      return highlightIndex
    }

    if (fuzzySearch) {
      highlightIndex = this.fuzzySearch(
        prefix,
        str,
        postfix,
        fuzzySearchOptions
      )
      if (highlightIndex !== -1) {
        return highlightIndex
      }
    }

    // eager search only works in (particular) browsers
    if (isBrowser && eagerSearchOptions) {
      highlightIndex = this.eagerSearch(
        prefix,
        str,
        postfix,
        eagerSearchOptions
      )
      if (highlightIndex !== -1) {
        return highlightIndex
      }
    }

    return highlightIndex
  }

  highlight(highlightIndex, options = {}) {
    // either containerId or content is required
    const containerId = options.containerId
    let content = options.content
    const highlightClass = options.highlightClass || 'highlight'
    // if true, return the highlighted content instead of highlighting on the page directly
    const returnContent = options.returnContent

    if (isBrowser && containerId) {
      content = document.getElementById(containerId).innerHTML
    }

    this.highlights[highlightIndex].highlightClass = highlightClass
    const openTag = this.createOpenTag(highlightIndex)
    const loc = this.adjustLoc(highlightIndex)
    let newContent = Highlighter.insert(content, openTag, loc[0])
    newContent = Highlighter.insert(
      newContent,
      CLOSE_HIGHLIGHT_TAG,
      loc[1] + openTag.length
    )
    this.highlights[highlightIndex].highlighted = true

    if (isBrowser && containerId && !returnContent) {
      document.getElementById(containerId).innerHTML = newContent
    } else {
      return newContent
    }
  }

  searchAndHighlight(str, options) {
    const highlightIndex = this.search(str, options.searchOptions)
    if (highlightIndex !== -1) {
      return {
        highlightIndex,
        content: this.highlight(highlightIndex, options.highlightOptions)
      }
    }
  }

  unhighlight(highlightIndex, options = {}) {
    // byStringOperation is used to decide whether the content is changed by string operation or dom operation
    const byStringOperation = options.byStringOperation
    // either containerId or content is required
    const containerId = options.containerId
    let content = options.content
    // if true, return the unhighlighted content instead of unhighlighting on the page directly
    const returnContent = options.returnContent

    this.highlights[highlightIndex].highlighted = false

    if (byStringOperation) {
      if (isBrowser && containerId) {
        content = document.getElementById(containerId).innerHTML
      }

      let newContent = content
      const loc = this.adjustLoc(highlightIndex)
      const openTagLength = this.getOpenTagLength(highlightIndex)
      const substr1 = newContent.substring(
        loc[0],
        loc[1] + openTagLength + CLOSE_HIGHLIGHT_TAG.length
      )
      const substr2 = newContent.substring(
        loc[0] + openTagLength,
        loc[1] + openTagLength
      )
      newContent = newContent.replace(substr1, substr2)

      if (returnContent) {
        return newContent
      } else {
        document.getElementById(containerId).innerHTML = newContent
      }
    } else if (isBrowser) {
      const elmId = this.highlightIdPattern + highlightIndex
      document.getElementById(elmId).outerHTML = document.getElementById(
        elmId
      ).innerHTML
      if (returnContent) {
        return document.getElementById(containerId).innerHTML
      }
    }
  }

  stripAndStoreHTMLTags() {
    let tag
    this.stripedHTML = this.originalContent
    const tagRegEx = /<[^>]+>/
    let indexInc = 0
    while ((tag = this.stripedHTML.match(tagRegEx))) {
      this.stripedHTML = this.stripedHTML.replace(tag, '')
      const tagLength = tag[0].length
      // tagLocations will be used in adjustLoc
      this.tagLocations.push([tag.index, tagLength, indexInc])
      indexInc += tagLength
    }
  }

  directSearch(prefix, str, postfix, directSearchOptions = {}) {
    const decodeHTML = directSearchOptions.decodeHTML
    // trim is enabled in direct search by default
    const trim =
      directSearchOptions.trim === undefined || directSearchOptions.trim

    const { newPrefix, newStr, newPostfix } = this.processStr(
      prefix,
      str,
      postfix,
      decodeHTML,
      trim
    )
    const strWithFixes = newPrefix + newStr + newPostfix

    let highlightIndex = -1
    const index = this.isHTML
      ? this.stripedHTML.indexOf(strWithFixes)
      : this.originalContent.indexOf(strWithFixes)
    if (index !== -1) {
      const loc = []
      loc[0] = index + newPrefix.length
      loc[1] = loc[0] + newStr.length
      highlightIndex = this.highlights.push({ loc }) - 1
    }
    return highlightIndex
  }

  eagerSearch(prefix, str, postfix, eagerSearchOptions = {}) {
    // both decodeHTML and trim are enabled in eager search by default
    const decodeHTML =
      eagerSearchOptions.decodeHTML === undefined ||
      eagerSearchOptions.decodeHTML
    const trim =
      eagerSearchOptions.trim === undefined || eagerSearchOptions.trim
    const containerId = eagerSearchOptions.containerId
    const threshold = eagerSearchOptions.threshold || 0.74

    const { newPrefix, newStr, newPostfix } = this.processStr(
      prefix,
      str,
      postfix,
      decodeHTML,
      trim
    )
    const strWithFixes = newPrefix + newStr + newPostfix

    let highlightIndex = -1
    // IE will not be considered
    if (window.find) {
      document.designMode = 'on'

      // step 1: ask the browser to highlight the found
      const sel = window.getSelection()
      sel.collapse(document.body, 0)
      while (window.find(strWithFixes)) {
        document.execCommand('hiliteColor', true, 'rgba(255, 255, 255, 0)')
        sel.collapseToEnd()
        // step 2: locate the found within the container where the highlighter is applied
        const found = document.querySelector(
          '#' +
            containerId +
            ' [style="background-color: rgba(255, 255, 255, 0);"]'
        )
        if (found) {
          const foundStr = found.innerHTML.replace(/<[^>]*>/g, '')
          const result = Highlighter.getBestSubstring(
            foundStr,
            newStr,
            threshold
          )
          if (result.similarity) {
            const index = this.isHTML
              ? this.stripedHTML.indexOf(foundStr)
              : this.originalContent.indexOf(foundStr)
            if (index !== -1) {
              highlightIndex =
                this.highlights.push({
                  loc: [index + result.loc[0], index + result.loc[1]]
                }) - 1
            }
          }
          break
        }
      }

      // step 3: remove the highlights created by the browser
      document.execCommand('undo')

      document.designMode = 'off'
    }
    return highlightIndex
  }

  // improve later***
  fuzzySearch(prefix, str, postfix, fuzzySearchOptions = {}) {
    // decodeHTML is enabled by default
    const decodeHTML =
      fuzzySearchOptions.decodeHTML === undefined ||
      fuzzySearchOptions.decodeHTML
    const trim = fuzzySearchOptions.trim
    let tbThreshold = fuzzySearchOptions.tbThreshold || 0.74
    let sbThreshold = fuzzySearchOptions.sbThreshold || 0.85
    const lenRatio = fuzzySearchOptions.lenRatio || 1.2
    const processSentence = fuzzySearchOptions.processSentence

    const { newPrefix, newStr, newPostfix } = this.processStr(
      prefix,
      str,
      postfix,
      decodeHTML,
      trim
    )

    let highlightIndex = -1
    const text = this.isHTML ? this.stripedHTML : this.originalContent
    // token-based
    if (newPrefix || newPostfix) {
      // step 1: find all indexes of str
      const strIndexes = []
      let i = -1
      while ((i = text.indexOf(newStr, i + 1)) !== -1) {
        strIndexes.push(i)
      }

      // step 2: find the index of the most similar "fragment" - the str with pre- and post- fixes
      let strIndex = -1
      const fragment = newPrefix + newStr + newPostfix
      for (const i of strIndexes) {
        const f =
          text.substring(i - newPrefix.length, i) +
          newStr +
          text.substring(
            i + newStr.length,
            i + newStr.length + newPostfix.length
          )
        const similarity = Highlighter.getSimilarity(f, fragment)
        if (similarity >= tbThreshold) {
          tbThreshold = similarity
          strIndex = i
        }
      }

      // step 3: check whether the most similar enough "fragment" is found, if yes return its location
      if (strIndex !== -1) {
        highlightIndex =
          this.highlights.push({ loc: [strIndex, strIndex + newStr.length] }) -
          1
      }
    }
    // sentence-based
    else {
      // step 1: sentenize the text if has not done so
      const sentences = this.sentences.length
        ? this.sentences
        : Highlighter.sentenize(text)

      // step 2 (for efficiency only): filter sentences by words of the str
      const words = newStr.split(/\s/)
      const filteredSentences = []
      for (const sentence of sentences) {
        for (const word of words) {
          if (sentence.raw.includes(word)) {
            filteredSentences.push(sentence)
            break
          }
        }
      }

      //step 2.5: remove text that must not be annotated
      if (processSentence) {
        const tagLocations = this.tagLocations
        const length = tagLocations.length
        if (length) {
          let index = 0
          for (const fs of filteredSentences) {
            let raw = fs.raw
            const loc = [fs.index, fs.index + raw.length]
            let locInc = 0
            for (let i = index; i < length; i++) {
              const tagLoc = tagLocations[i]
              if (tagLoc[0] >= loc[0] && tagLoc[0] <= loc[1]) {
                const tag = this.originalContent.substring(
                  tagLoc[0] + tagLoc[2],
                  tagLoc[0] + tagLoc[2] + tagLoc[1]
                )
                const insertIndex = tagLoc[0] + locInc - loc[0]
                raw = raw.slice(0, insertIndex) + tag + raw.slice(insertIndex)
                locInc += tagLoc[1]
              } else if (tagLoc[0] > loc[1]) {
                index = i - 1
                break
              }
            }

            raw = processSentence(raw)
            raw = raw.replace(/(<([^>]+)>)/gi, '')

            const copy = fs.raw
            // update the sentence if it got reduced
            if (copy !== raw) {
              fs.raw = raw
              fs.index = fs.index + copy.indexOf(raw)
            }
          }
        }
      }

      // step 3: find the sentence that includes the most similar str
      let bestResult = null
      let mostPossibleSentence = null
      filteredSentences.forEach((sentence, index) => {
        let result = Highlighter.getBestSubstring(
          sentence.raw,
          newStr,
          sbThreshold,
          lenRatio
        )
        if (result.similarity) {
          sbThreshold = result.similarity
          bestResult = result
          mostPossibleSentence = sentence
        } else if (index !== filteredSentences.length - 1) {
          // combine two sentences to reduce the inaccuracy of sentenizing text
          result = Highlighter.getBestSubstring(
            sentence.raw + filteredSentences[index + 1].raw,
            newStr,
            sbThreshold,
            lenRatio
          )
          if (result.similarity) {
            sbThreshold = result.similarity
            bestResult = result
            mostPossibleSentence = filteredSentences[index]
          }
        }
      })

      // step 4: if such sentence is found, derive and return the location of the most similar str
      if (bestResult) {
        let index = mostPossibleSentence.index
        highlightIndex =
          this.highlights.push({
            loc: [index + bestResult.loc[0], index + bestResult.loc[1]]
          }) - 1
      }
    }
    return highlightIndex
  }

  createOpenTag(highlightIndex) {
    const highlightIdPattern = this.highlightIdPattern
    const highlightClass = this.highlights[highlightIndex].highlightClass
    return OPEN_HIGHLIGHT_TAG(highlightIdPattern, highlightClass).replace(
      highlightIdPattern,
      highlightIdPattern + highlightIndex
    )
  }

  // improve later***
  adjustLoc(highlightIndex) {
    const highlightLoc = this.highlights[highlightIndex].loc
    const locInc = [0, 0]

    // step 1: check locations of tags
    const tagLocations = this.tagLocations
    const length = tagLocations.length
    for (let i = 0; i < length; i++) {
      const tagLoc = tagLocations[i]
      if (highlightLoc[1] < tagLoc[0]) {
        break
      } else if (highlightLoc[1] === tagLoc[0]) {
        const tag = this.originalContent.substring(
          tagLoc[0] + tagLoc[2],
          tagLoc[0] + tagLoc[2] + tagLoc[1]
        )
        if (tag.startsWith('</')) {
          locInc[1] += tagLoc[1]
        }
      } else if (highlightLoc[1] > tagLoc[0]) {
        locInc[1] += tagLoc[1]
        if (highlightLoc[0] === tagLoc[0]) {
          const tag = this.originalContent.substring(
            tagLoc[0] + tagLoc[2],
            tagLoc[0] + tagLoc[2] + tagLoc[1]
          )
          if (tag.startsWith('</')) {
            locInc[0] += tagLoc[1]
          } else {
            let included = false

            let requiredCloseTagNumber = 1
            let closeTagCount = 0
            for (let i2 = i + 1; i2 < tagLocations.length; i2++) {
              const tagLoc2 = tagLocations[i2]
              if (highlightLoc[1] <= tagLoc2[0]) {
                break
              } else {
                const tag2 = this.originalContent.substring(
                  tagLoc2[0] + tagLoc2[2],
                  tagLoc2[0] + tagLoc2[2] + tagLoc2[1]
                )
                const tagType = tag
                  .split(' ')[0]
                  .split('<')[1]
                  .split('>')[0]
                if (tag2.startsWith('<' + tagType)) {
                  requiredCloseTagNumber++
                } else if (tag2.startsWith('</' + tagType)) {
                  closeTagCount++
                }
                if (requiredCloseTagNumber === closeTagCount) {
                  included = true
                  break
                }
              }
            }

            if (!included) {
              locInc[0] += tagLoc[1]
            }
          }
        } else if (highlightLoc[0] > tagLoc[0]) {
          locInc[0] += tagLoc[1]
        }
      }
    }

    // step 2: check locations of other highlights
    this.highlights.forEach((highlight, highlightIndex) => {
      if (highlight.highlighted) {
        const openTagLength = this.getOpenTagLength(highlightIndex)
        const closeTagLength = CLOSE_HIGHLIGHT_TAG.length
        const loc = highlight.loc
        if (highlightLoc[0] >= loc[1]) {
          locInc[0] += openTagLength + closeTagLength
          locInc[1] += openTagLength + closeTagLength
        } else if (
          highlightLoc[0] < loc[1] &&
          highlightLoc[0] > loc[0] &&
          highlightLoc[1] > loc[1]
        ) {
          locInc[0] += openTagLength
          locInc[1] += openTagLength + closeTagLength
        } else if (highlightLoc[0] <= loc[0] && highlightLoc[1] >= loc[1]) {
          locInc[1] += openTagLength + closeTagLength
        } else if (
          highlightLoc[0] < loc[0] &&
          highlightLoc[1] > loc[0] &&
          highlightLoc[1] < loc[1]
        ) {
          locInc[1] += openTagLength
        } else if (highlightLoc[0] >= loc[0] && highlightLoc[1] <= loc[1]) {
          locInc[0] += openTagLength
          locInc[1] += openTagLength
        }
      }
    })

    return [highlightLoc[0] + locInc[0], highlightLoc[1] + locInc[1]]
  }

  getOpenTagLength(highlightIndex) {
    const highlightClass = this.highlights[highlightIndex].highlightClass
    // an easy way to derive the length
    return (
      OPEN_HIGHLIGHT_TAG(this.highlightIdPattern, highlightClass) +
      highlightIndex
    ).length
  }

  processStr(prefix, str, postfix, decodeHTML, trim) {
    let newPrefix = prefix
    let newStr = str
    let newPostfix = postfix

    if (decodeHTML) {
      newPrefix = Highlighter.decodeHTML(newPrefix)
      newStr = Highlighter.decodeHTML(newStr)
      newPostfix = Highlighter.decodeHTML(newPostfix)
    }
    if (trim) {
      newPrefix = newPrefix.replace(/^\s+/, '')
      newPostfix = newPostfix.replace(/\s+$/, '')
    }

    return { newPrefix, newStr, newPostfix }
  }

  static insert(str1, str2, index) {
    return str1.slice(0, index) + str2 + str1.slice(index)
  }

  static decodeHTML(str) {
    // better to lazy load entities***
    // if (isBrowser) {
    // const textarea = document.createElement('textarea')
    // textarea.innerHTML = str
    // return textarea.value
    // } else {
    // const entities = () => import('entities')
      const entities = require('entities')
      return entities.decodeHTML(str)
    // }
  }

  static sentenize(text) {
    const options = {
      newline_boundaries: false,
      html_boundaries: false,
      sanitize: false,
      allowed_tags: false,
      preserve_whitespace: true,
      abbreviations: null
    }
    return getSentences(text, options).map(raw => {
      // can tokenizer return location directly***
      const index = text.indexOf(raw)
      return { raw, index }
    })
  }

  // static getBestSubstring(str, substr, threshold, lenRatio) {
  //   let result = {}

  //   let similarity = Highlighter.getSimilarity(str, substr)
  //   if (similarity >= threshold) {
  //     // step 1: derive best substr
  //     const wordsAndSpaces = str.split(/(\s+)/)
  //     let newLoc = [0, str.length]
  //     while (wordsAndSpaces.length) {
  //       // assuming str has been trimmed
  //       const firstWord = wordsAndSpaces.shift()
  //       const firstSpace = wordsAndSpaces.length
  //         ? wordsAndSpaces.shift()
  //         : undefined

  //       const newStr = wordsAndSpaces.join('')
  //       let newSimilarity = Highlighter.getSimilarity(newStr, substr)
  //       if (newSimilarity < similarity) {
  //         if (firstSpace !== undefined) {
  //           wordsAndSpaces.unshift(firstSpace)
  //         }
  //         wordsAndSpaces.unshift(firstWord)
  //         const lastWord = wordsAndSpaces.pop()
  //         const lastSpace = wordsAndSpaces.length
  //           ? wordsAndSpaces.pop()
  //           : undefined
  //         newSimilarity = Highlighter.getSimilarity(
  //           wordsAndSpaces.join(''),
  //           substr
  //         )
  //         if (newSimilarity < similarity) {
  //           if (lastSpace !== undefined) {
  //             wordsAndSpaces.push(lastSpace)
  //           }
  //           wordsAndSpaces.push(lastWord)
  //         } else {
  //           similarity = newSimilarity
  //           newLoc[1] -= lastWord.length
  //           if (lastSpace !== undefined) {
  //             newLoc[1] -= lastSpace.length
  //           }
  //         }
  //       } else {
  //         similarity = newSimilarity
  //         newLoc[0] += firstWord.length
  //         if (firstSpace !== undefined) {
  //           newLoc[0] += firstSpace.length
  //         }
  //       }
  //     }

  //     const bestSubstr = wordsAndSpaces.join('')

  //     // step 2: return the best substr and its loc if found and if it meets the threshold and the length ratio
  //     if (!lenRatio || bestSubstr.length / substr.length <= lenRatio) {
  //       const loc = []
  //       loc[0] = newLoc[0]
  //       loc[1] = newLoc[1]
  //       result = { similarity, loc }
  //     }
  //   }

  //   return result
  // }
  static getBestSubstring(str, substr, threshold, lenRatio) {
    let result = {}

    let similarity = Highlighter.getSimilarity(str, substr)
    if (similarity >= threshold) {
      // step 1: derive best substr
      const words = str.split(' ')
      while (words.length) {
        const firstWord = words.shift()
        const newStr = words.join(' ')
        let newSimilarity = Highlighter.getSimilarity(newStr, substr)
        if (newSimilarity < similarity) {
          words.unshift(firstWord)
          const lastWord = words.pop()
          newSimilarity = Highlighter.getSimilarity(words.join(' '), substr)
          if (newSimilarity < similarity) {
            words.push(lastWord)
            break
          } else {
            similarity = newSimilarity
          }
        } else {
          similarity = newSimilarity
        }
      }
      const bestSubstr = words.join(' ')

      // step 2: return the best substr and its loc if found and if it meets the threshold and the length ratio
      if (!lenRatio || bestSubstr.length / substr.length <= lenRatio) {
        const loc = []
        loc[0] = str.indexOf(bestSubstr)
        loc[1] = loc[0] + bestSubstr.length
        result = { similarity, loc }
      }
    }

    return result
  }

  static getSimilarity(str1, str2) {
    if (str1 === str2) return 1
    // set str2 to denominator
    return Highlighter.lcsLength(str1, str2) / str2.length
  }

  static lcsLength(str1, str2) {
    const length1 = str1.length
    const length2 = str2.length
    const x = str1.split('')
    const y = str2.split('')

    const c = Array(length1 + 1).fill(Array(length2 + 1).fill(0))
    for (let i = 1; i <= length1; i++) {
      for (let j = 1; j <= length2; j++) {
        c[i][j] =
          x[i - 1] === y[j - 1]
            ? c[i - 1][j - 1] + 1
            : Math.max(c[i][j - 1], c[i - 1][j])
      }
    }
    return c[length1][length2]
  }
}

export default Highlighter
