import rp from 'request-promise'
import $ from 'cheerio'
import Annotations from './Annotation'

// config this
const isHTML = true
const source = 'MED'
const id = 31894724
const abstractOnly = false

const fulltextRequest = `http://europepmc.org/article/${source}/${id}?javascript_support=no`

const startTime = Date.now()
rp(fulltextRequest).then(html => {
  console.log('Time for requesting article:' + (Date.now() - startTime) / 1000 + 's')  
  const content = $(
    abstractOnly ? '#article--abstract--content' : '#fulltextcontent',
    html
  ).html()
  const annotations = new Annotations({
    content,
    isHTML
  })
  annotations
    .loadAnnotations({
      source,
      id,
      abstractOnly
    })
    .then(() => {
      console.log('Total time:' + (Date.now() - startTime) / 1000 + 's')
    })
})
