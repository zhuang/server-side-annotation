// what is left****
// temp sentence
// when use sentence/token based
// callback in the adjustLoc method

import Highlighter from './text-highlight'

const fetchAnnotations = (source, id, abstractOnly, provider) => {
  let req = 'http://europepmc.org/api/get/annotationsApi?'
  req += 'source=' + source
  req += '&id=' + id
  req += abstractOnly ? '&section=Abstract' : ''
  req += '&provider=' + encodeURIComponent(provider)

  const rp = require('request-promise')
  return rp(req)
}

class Annotations {
  static highlightIdPattern = 'annotation-'
  static providers = [
    'Europe PMC',
    'nactem',
    'OpenTargets',
    'DisGeNET',
    'PubTator_NCBI',
    'IntAct',
    'HES-SO_SIB',
    'NTNU/BSC',
    'OntoGene'
  ]

  static highlightClass = 'scilite-annotation'

  static removeAnnotationTags(text) {
    const regex = new RegExp(
      '<span id="' + Annotations.highlightIdPattern + '([^>]+)>(.+)</span>',
      'gi'
    )
    return text.replace(regex, '$2')
  }

  static getTagLinkLabel(uri, provider) {
    let label = 'Details'

    if (uri.includes('http://purl.uniprot.org/uniprot/')) label = 'Uniprot'
    else if (uri.includes('http://identifiers.org/taxonomy/'))
      label = 'Taxonomy'
    else if (uri.includes('http://purl.obolibrary.org/obo/CHEBI_'))
      label = 'ChEBI'
    else if (
      uri.includes('http://purl.obolibrary.org/obo/GO:') ||
      uri.includes('http://identifiers.org/go/GO:')
    )
      label = 'GO Term'
    else if (uri.includes('http://linkedlifedata.com/resource/umls-concept/'))
      label = 'Linked Life Data'
    else if (uri.includes('http://www.ebi.ac.uk/efo/')) label = 'EBI'
    else if (uri.includes('http://identifiers.org/')) {
      if (uri.includes('http://identifiers.org/pdb/')) label = 'PDB'
      else if (uri.includes('http://identifiers.org/pdb/')) label = 'TreeFam'
      else if (uri.includes('http://identifiers.org/uniprot/'))
        label = 'UniProt'
      else if (uri.includes('http://identifiers.org/arrayexpress/'))
        label = 'ArrayExpress'
      else if (uri.includes('http://identifiers.org/bioproject/'))
        label = 'BioProject'
      else if (uri.includes('http://identifiers.org/biosample/'))
        label = 'BioSample'
      else if (uri.includes('http://identifiers.org/clinicaltrials/'))
        label = 'Clinical Trial'
      else if (uri.includes('http://identifiers.org/doi/')) label = 'DOI'
      else if (uri.includes('http://identifiers.org/ega.study/')) label = 'EGA'
      else if (uri.includes('http://identifiers.org/emdb/')) label = 'EMDB'
      else if (uri.includes('http://identifiers.org/ensembl/'))
        label = 'Ensembl'
      else if (uri.includes('http://identifiers.org/ena.embl/')) label = 'ENA'
      else if (uri.includes('http://identifiers.org/go/'))
        label = 'Gene Ontology'
      else if (uri.includes('http://identifiers.org/interpro/'))
        label = 'InterPro'
      else if (uri.includes('http://identifiers.org/omim/')) label = 'OMIM'
      else if (uri.includes('http://identifiers.org/pfam/')) label = 'Pfam'
      else if (uri.includes('http://identifiers.org/proteomexchange/'))
        label = 'ProteomeXchange'
      else if (uri.includes('http://identifiers.org/refseq/')) label = 'RefSeq'
      else if (uri.includes('http://identifiers.org/dbsnp/')) label = 'RefSNP'
      else if (uri.includes('identifiers.org/miriam.resource')) label = 'PDB'
    } else if (provider === 'nactem') label = 'Uniprot'
    else if (provider === 'HES-SO_SIB') label = 'Uniprot'
    else if (provider === 'OpenTargets') label = 'OpenTargets'
    else if (provider === 'DisGeNET') {
      label = 'DisGeNET'
      if (uri.indexOf('ncbi') > -1) label = 'NCBI'
      else if (uri.indexOf('umls') > -1) label = 'UMLS'
    } else if (provider === 'PubTator_NCBI') label = 'NCBI'
    else if (provider === 'IntAct') label = 'IntAct'
    else if (provider === 'NTNU/BSC') label = 'Ensembl'

    return label
  }

  static processSentence(raw) {
    let newRaw = raw
    newRaw = newRaw.replace(
      /(^<h3>(Abstract|ABSTRACT|Introduction|Funding Statement|Disclosure statement|Conclusions|Background|Results|Discussuion|Methods|Abbreviations|Competing interests|Acknowledgement|References|Authors' contributions)<\/h3>)/gi,
      ''
    )
    newRaw = newRaw.replace(
      /(<h3>(Abstract|ABSTRACT|Introduction|Funding Statement|Disclosure statement|Conclusions|Background|Results|Discussuion|Methods|Abbreviations|Competing interests|Acknowledgement|References|Authors' contributions)<\/h3>+$)/gi,
      ''
    )
    newRaw = newRaw.replace(
      /(^<h2([^>]+)>(Abstract|ABSTRACT|Introduction|Funding Statement|Disclosure statement|Conclusions|Background|Results|Discussuion|Methods|Abbreviations|Competing interests|Acknowledgement|References|Authors' contributions)<\/h2>)/gi,
      ''
    )
    newRaw = newRaw.replace(
      /(<h2([^>]+)>(Abstract|ABSTRACT|Introduction|Funding Statement|Disclosure statement|Conclusions|Background|Results|Discussuion|Methods|Abbreviations|Competing interests|Acknowledgement|References|Authors' contributions)<\/h2>+$)/gi,
      ''
    )
    return newRaw
  }

  static processText(text) {
    return text
  }

  // --------1. intitialization--------
  constructor({ content, containerId, isHTML, disableLocation }) {
    // category: {
    //  title
    //  type
    //  labels
    // }

    // label: {
    //  title
    //  annotations
    //  providers?
    //  selected (if not disableLocation)
    //  nextAnnotationIndex (if not disableLocation)
    // }

    // annotation: {
    //  id
    //  prefix
    //  exact
    //  postfix
    //  providers
    //  highlightIndex (if not disableLocation)
    // }

    // provider: {
    //  provider
    //  tags
    // }
    this.categories = [
      {
        title: 'Accession Numbers',
        type: 'Accession Numbers',
        labels: []
      },
      {
        title: 'Genes/Proteins',
        type: 'Gene_Proteins',
        labels: []
      },
      {
        title: 'Diseases',
        type: 'Diseases',
        labels: []
      },
      {
        title: 'Organisms',
        type: 'Organisms',
        labels: []
      },
      {
        title: 'Chemicals',
        type: 'Chemicals',
        labels: []
      },
      {
        title: 'Clinical Drug',
        type: 'Clinical Drug',
        labels: []
      },
      {
        title: 'Gene Ontology',
        type: 'Gene Ontology',
        labels: []
      },
      {
        title: 'Experimental Methods',
        type: 'Experimental Methods',
        labels: []
      },
      {
        title: 'Resources',
        type: 'Resources',
        labels: []
      },
      // - need more logic to determine the category
      {
        title: 'Gene-Disease OpenTargets',
        type: 'Gene Disease OpenTargets',
        labels: []
      },
      {
        title: 'Gene-Disease DisGeNET',
        type: 'Gene Disease DisGeNET',
        labels: []
      },
      // -
      {
        title: 'Gene Function',
        type: 'Gene Function',
        labels: []
      },
      {
        title: 'Genetic mutations',
        type: 'Gene Mutations',
        labels: []
      },
      {
        title: 'Sequence',
        type: 'Sequence',
        labels: []
      },
      {
        title: 'Transcription factor – Target gene',
        type: 'Transcription factor - Target gene',
        labels: []
      },
      {
        title: 'Protein-protein Interactions',
        type: 'Protein Interaction',
        labels: []
      },
      {
        title: 'Phosphorylation Event',
        type: 'Biological Event',
        labels: []
      },
      {
        title: 'Molecular Process',
        type: 'Molecular Process',
        labels: []
      },
      {
        title: 'Cell',
        type: 'Cell',
        labels: []
      },
      {
        title: 'Cell Line',
        type: 'Cell Line',
        labels: []
      },
      {
        title: 'Organ Tissue',
        type: 'Organ Tissue',
        labels: []
      }
    ]

    this.disableLocation = disableLocation

    // no need to search/highlight for bio-entities
    if (!disableLocation) {
      const options = {
        isHTML,
        highlightIdPattern: Annotations.highlightIdPattern
      }
      if (content) {
        options.content = content
      }
      // require highlighter and containerId if not disableLocation
      this.highlighter = new Highlighter(options)
      this.containerId = containerId
    }
  }
  // --------1 end--------

  // --------2. load annotations--------
  loadAnnotations({ source, id, abstractOnly }) {
    return Promise.all(
      Annotations.providers.map(provider =>
        fetchAnnotations(source, id, abstractOnly, provider).then(response => {
          if (response.length) {
            this._assignCategory(provider, JSON.parse(response)[0].annotations)
          }
        })
      )
    ).then(() => {
      // sort labels of each category
      this.categories.forEach(cat => {
        cat.labels.sort(
          (l1, l2) => l2.annotations.length - l1.annotations.length
        )
      })
    })
  }

  _assignCategory(provider, annotations) {
    annotations.forEach(annotation => {
      const catIndex = this.categories.findIndex(
        category =>
          category.type ===
          (annotation.type === 'Gene Disease Relationship'
            ? provider === 'OpenTargets'
              ? 'Gene Disease OpenTargets'
              : 'Gene Disease DisGeNET'
            : annotation.type)
      )
      if (catIndex !== -1) {
        this._addAnnotation(catIndex, provider, annotation)
      }
    })
  }

  _addAnnotation(catIndex, provider, annotation) {
    const { id, prefix, exact, postfix, tags } = annotation
    const { containerId, highlighter, disableLocation } = this

    const newPrefix = Annotations.processText(prefix)
    const newExact = Annotations.processText(exact)
    const newPostfix = Annotations.processText(postfix)

    let highlightIndex = -1

    if (!disableLocation) {
      // search options
      const options = {
        prefix: newPrefix,
        postfix: newPostfix,
        // eagerSearchOptions: { containerId },
        fuzzySearch: true,
        fuzzySearchOptions: {
          processSentence: Annotations.processSentence
        }
      }

      highlightIndex = highlighter.search(newExact, options)
    }

    if (disableLocation || highlightIndex !== -1) {
      // the annotation to be added into the label
      const annotationToAdd = {
        id,
        prefix,
        exact,
        postfix,
        providers: [
          {
            provider,
            tags
          }
        ]
      }
      if (!disableLocation) {
        annotationToAdd.highlightIndex = highlightIndex
      }

      const labelName = Highlighter.decodeHTML(exact)
      // the label title displayed on the side bar can be different to the label name in sentence annotations
      const labelTitle = this._getLabelTitle(labelName, tags)
      const labels = this.categories[catIndex].labels
      let label = labels.find(label => label.title === labelTitle)

      // add new label only when the same label does not exist
      if (label) {
        const annotations = label.annotations
        // if disableLocation is true, it means that the annotation cannot be highlighted, which means that it is impossible to know whether the annotation is the same as one of the others
        const sameAnnotation = disableLocation
          ? false
          : annotations.find(a => {
              const loc = highlighter.highlights[a.highlightIndex].loc
              const loc2 = highlighter.highlights[highlightIndex].loc
              return loc[0] === loc2[0] && loc[1] === loc2[1]
            })

        // add new annotation only when the same annotation does not exist
        if (sameAnnotation) {
          const sameProvider = sameAnnotation.providers.find(
            p => p.provider === provider
          )

          // update the same annotation only when the provider is different
          if (!sameProvider) {
            sameAnnotation.providers.push({
              provider,
              tags
            })
          }
        } else {
          annotations.push(annotationToAdd)
        }
      } else {
        label = {
          title: labelTitle,
          annotations: [annotationToAdd],
          providers: []
        }
        if (!disableLocation) {
          label = Object.assign(label, {
            selected: false,
            nextAnnotationIndex: false
          })
        }
        labels.push(label)
      }

      // update the providers in the label
      const sameProvider = label.providers.find(p => p.provider === provider)
      if (sameProvider) {
        const sameTags = tags.every(tag =>
          sameProvider.tags.find(t => t.name === tag.name && t.uri === tag.uri)
        )
        if (!sameTags) {
          sameProvider.tags = sameProvider.tags.concat(tags)
        }
      } else {
        label.providers.push({ provider, tags })
      }
    }
  }

  _getLabelTitle(labelName, tags) {
    let labelTitle = labelName
    if (tags.length === 2) {
      let firstTagIndex = 0
      for (let i = 0; i < tags.length; i++) {
        if (
          tags[i].uri.includes('ncbigene') ||
          tags[i].uri.includes('uniprot')
        ) {
          firstTagIndex = i
          break
        }
      }
      labelTitle =
        tags[firstTagIndex].name + ' - ' + tags[!firstTagIndex ? 1 : 0].name
    }
    return labelTitle
  }
  // --------2 end--------

  // --------3. interact with annotations that can be located--------
  clearAnnotations() {
    this.categories.forEach(cat => {
      cat.labels.forEach(label => {
        if (label.selected) {
          label.annotations.forEach(annotation => {
            this.highlighter.highlights[
              annotation.highlightIndex
            ].highlighted = false
          })
          label.selected = false
          label.nextAnnotationIndex = false
        }
      })
    })
    return this.highlighter.originalContent
  }

  selectCategory(category, content) {
    let newContent = content

    const selected = Boolean(category.labels.find(label => !label.selected))
    category.labels
      .filter(label => label.selected !== selected)
      .forEach(label => {
        newContent = this.selectLabel(label, newContent)
      })

    return newContent
  }

  selectLabel(label, content) {
    let newContent = content

    const { highlighter } = this
    label.selected = !label.selected
    const annotations = label.annotations
    const highlightIndexes = annotations.map(a => a.highlightIndex)
    if (label.selected) {
      // sort annotations first time of selecting
      if (
        highlighter.highlights[annotations[0].highlightIndex].highlighted ===
        undefined
      ) {
        annotations.sort((a1, a2) => {
          const loc1 = highlighter.highlights[a1.highlightIndex].loc
          const loc2 = highlighter.highlights[a2.highlightIndex].loc
          return loc1[0] - loc2[0]
        })
      }

      highlightIndexes.forEach(highlightIndex => {
        const options = {
          content: newContent,
          highlightClass: Annotations.highlightClass,
          returnContent: true
        }
        newContent = highlighter.highlight(highlightIndex, options)
      })
    } else {
      highlightIndexes.forEach(highlightIndex => {
        const options = {
          content: newContent,
          returnContent: true,
          byStringOperation: true
        }
        newContent = highlighter.unhighlight(highlightIndex, options)
      })
    }

    // is it the best place
    label.nextAnnotationIndex = false

    return newContent
  }

  goToAnnotation(label, prev) {
    if (label.nextAnnotationIndex === false) {
      label.nextAnnotationIndex = 0
    } else if (prev) {
      if (label.nextAnnotationIndex !== 0) {
        label.nextAnnotationIndex--
      }
    } else {
      label.nextAnnotationIndex =
        label.nextAnnotationIndex === label.annotations.length - 1
          ? 0
          : ++label.nextAnnotationIndex
    }
  }
}

export default Annotations
